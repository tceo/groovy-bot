package tceo.vdds

class Database {
    String name

    private File offsetTable
    private RandomAccessFile database

    private Map<String, DataType> fields
    // Integers should do fine for now.
    private Map<String, List<Integer>> offsets

    Database(String name) {
        this.name = name
    }

    void load(String path, Map<String, DataType> fields) {
        this.offsetTable = new File("$path/${name}.vdot")
        // Read and Write, write all data to file synchronously
        this.database = new RandomAccessFile("$path/${name}.vddb", "rwd")
        this.fields = fields
        
    }
}