package tceo.commands

import tceo.BotMain

class CommandRegistery {
    private HashMap<String, Command> aliasMap = new HashMap<>()
    private HashMap<String, Command[]> modules = new HashMap<>()
    private ArrayList<Command> uniqueCommands = new ArrayList<>()
    private ArrayList<String> categories = new ArrayList<>()

    void registerCommand(Command command, String module) {
        for (String name : command.name) {
            def m = name =~ "([^\\w])+"
            if(!m)
                aliasMap.put(name, command)
        }
        BotMain.Log("Registered command: $command.name")
        uniqueCommands.add(command)
        if(!modules.get(module))
            modules[module] = [] as Command[]
        modules[module] += command
        if(categories.contains(command.category))
            categories.add(command.category)
    }

    Exception runCommand(String name, CommandContext ctx) {
        try {
            if(aliasMap.containsKey(name))
                new Thread(aliasMap.get(name).getRunnableCommand(ctx)).start()
            return null
        } catch (Exception e) {
            return e
        }
    }

    Exception runCommand(Command command, CommandContext ctx) {
        try {
            new Thread(command.getRunnableCommand(ctx)).start()
            return null
        } catch (Exception e) {
            return e
        }
    }

    ArrayList<Command> getCommandsByCategory(String category) {
        if(categories.contains(category))
            return null

        def list = new ArrayList<Command>()
        for(Command c : uniqueCommands) {
            if(c.category == category)
                list.add(c)
        }

        return list
    }

    Command getCommandByName(String name) {
        return aliasMap.get(name)
    }

    boolean doesCommandExist(String name) {
        return aliasMap.containsKey(name)
    }

    ArrayList<Command> getUniqueCommands() {
        return uniqueCommands
    }

    void clear() {
        aliasMap.clear()
        modules.clear()
        uniqueCommands.clear()
        categories.clear()
    }
}
