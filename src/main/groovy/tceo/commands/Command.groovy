package tceo.commands

import org.codehaus.groovy.runtime.StackTraceUtils
import tceo.BotMain
import tceo.essnt.SyntaxNode
import tceo.essnt.SyntaxParser

class Command {
    String[] name
    String module
    String description = "This command does not provide help"
    String category = "Uncategorized"
    SyntaxNode syntax

    public Closure<CommandContext> runner = {
        println "overwrite me!"
    }

    Command(String[] name, String syntax, String description, Closure<CommandContext> runner) {
        module = StackTraceUtils.sanitize(new Exception()).stackTrace[1].fileName.replace(".groovy", "")
        this.name = name
        this.syntax = SyntaxParser.createSyntaxTree(syntax)
        this.description = description
        this.runner = runner

        BotMain.registry.registerCommand(this, module)
    }

    Command(String[] name, String syntax, String description, Closure<CommandContext> runner, String category) {
        module = StackTraceUtils.sanitize(new Exception()).stackTrace[1].fileName.replace(".groovy", "")
        this.name = name
        this.syntax = SyntaxParser.createSyntaxTree(syntax)
        this.description = description
        this.category = category
        this.runner = runner

        BotMain.registry.registerCommand(this, module)
    }

    RunnableCommand getRunnableCommand(CommandContext ctx) {
        return new RunnableCommand(runner.&call, ctx)
    }
}
