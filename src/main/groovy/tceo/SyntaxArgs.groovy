package tceo

import tceo.essnt.*
import net.dv8tion.jda.api.entities.User

class SyntaxArgs {
    static registerArgTypes() {
        SyntaxArgType.register(new SyntaxArgType("Integer", Integer.class,
        { String toValidate, Object[] args ->
            return toValidate.isInteger()
        }, {String toConvert ->
            return toConvert as Integer
        }))

        SyntaxArgType.register(new SyntaxArgType("String", String.class,
        { String toValidate, Object[] args ->
            return true
        }, {String toConvert ->
            return toConvert as String
        }))

        SyntaxArgType.register(new SyntaxArgType("ExactString", String.class,
        { String toValidate, Object[] args ->
            return toValidate == args[0]
        }, {String toConvert ->
            return toConvert as String
        }))

        SyntaxArgType.register(new SyntaxArgType("User", User.class,
        { String toValidate, Object[] args ->
            if(!toValidate.startsWith("<@!")) return false
            return BotMain.client.getUserById(toValidate[3..-2]) as boolean
        }, {String toConvert ->
            return BotMain.client.getUserById(toConvert[3..-2])
        }))
    }
}
