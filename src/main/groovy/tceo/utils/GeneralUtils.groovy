package tceo.utils

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.User

class GeneralUtils {
    static void deleteMessageAfter(Message message, long time) {
        new Timer().schedule({-> message.delete().queue()}, time)
    }

    static User getUserByName(String name, Guild guild) {
        for(Member m : guild.getMembers())
            if(m.effectiveName.toLowerCase().startsWith(name.toLowerCase())) return m.user
        return null
    }

    static boolean prob(float percentage) {
        return Math.random() <= (percentage / 100)
    }
}
