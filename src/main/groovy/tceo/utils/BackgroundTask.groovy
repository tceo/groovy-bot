package tceo.utils

import java.time.Duration

class BackgroundTask {
    Closure runner
    Duration time
    short strikes

    BackgroundTask(Closure runner, Duration time) {
        this.time = time
        this.runner = runner
    }
}
