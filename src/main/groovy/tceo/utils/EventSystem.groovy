package tceo.utils

import org.codehaus.groovy.runtime.StackTraceUtils
import tceo.BotMain

class EventSystem {
    private Map<String, Map<String, Closure[]>> eventHandlers = new HashMap<>()
    def paused = false

    synchronized def plus(def arg) {
        def module = StackTraceUtils.sanitize(new Throwable()).stackTrace[2].className
        if (arg instanceof List
                && arg.size() == 2
                && arg[0] instanceof String
                && arg[1] instanceof Closure) {
            BotMain.Log("$module registered event handler for ${arg[0]}.", Logger.Severity.DBG)
            if (eventHandlers[arg[0]]) {
                if(!eventHandlers[arg[0]][module])
                    eventHandlers[arg[0]][module] = []
                eventHandlers[arg[0]][module] += arg[1]
            } else
                eventHandlers[arg[0]] = ["$module": [arg[1]]]
        } else
            BotMain.Log("Invalid event registration", Logger.Severity.ERR)
    }
    def on = this.&plus

    synchronized def leftShift(def arg) {
        if (paused) return

        if (arg instanceof List) {
            def event = arg[0] as String
            arg = arg.size() > 1 ? arg[1..-1] : []
            eventHandlers[event].each {
                k, v ->
                    v.each {c -> c(*arg)}
            }
        }
    }
    def fire = this.&leftShift

    def remove(def key) {
        eventHandlers.remove(key)
    }

    def clear() {
        eventHandlers.clear()
    }
}
